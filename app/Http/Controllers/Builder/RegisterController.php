<?php

namespace App\Http\Controllers\Builder;

use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    use \WBuilder\Core\Traits\RegisterController;
}
