<?php

namespace App\Http\Controllers\Builder;

use App\Http\Controllers\Controller;

class VerificationController extends Controller
{
    use \WBuilder\Core\Traits\VerificationController;
}
