<?php

namespace App\Http\Controllers\Builder;

use App\Http\Controllers\Controller;

class ShopController extends Controller
{
    use \WBuilder\Core\Traits\ShopController;
}
