<?php

namespace App\Http\Controllers\Builder;

use App\Http\Controllers\Controller;

class ResetPasswordController extends Controller
{
    use \WBuilder\Core\Traits\ResetPasswordController;
}
