<?php

return [
    "title" => "shop_v1_theme",
    "dashboard" => "Dashboard",
    "my.account" => 'My Account',
    'wishlist' => 'Wishlist',
    'compare' => 'Compare',
    'search.products' => 'Search Products',
    'logout' => 'Logout',
    'total' => 'Total',
    'view.cart' => 'View Cart',
    'view' => 'View',
    'checkout' => 'Checkout',
    'home' => 'Home',
    'welcome.guest' => 'Welcome Guest !',
    'shop.now' => 'Shop Now',
    'view.all' => 'View All',
    'product.id' => 'Product ID',
    'category' => 'Categroy',
    'tags' => 'Tags',
    'available' => 'Available',
    'color' => 'Color',
    'size' => 'Size',
    'add.to.cart' => 'Add to Cart',
    'add.to.wishlist' => 'Add to Wishlist',
    'add.to.compare' => 'Add to Compare',
    'discover.now' => 'Discover Now',
    'special.offers' => 'Special Offers',
    'newsletter' => 'Newsletter',
    'enter.your.email' => 'Enter Your Email',
    'submit' => 'Submit',
    'products' => 'Products',
    'news' => 'News',
    'contact' => 'Contact',
    'about' => 'About',
    'prev' => 'Prev',
    'previous' => 'Previous',
    'next' => 'Next',
    'top.offers' => 'Top Offers',
    'of' => 'of',
    'showing' => 'Showing',
    'sort.by' => 'Sort by',
    'most.rated' => 'Most Rated',
    'date' => 'Date',
    'product.categories' => 'Product Categories',
    'product.price' => 'Product Price',
    'filter.by.color' => 'Filter By Color',
    'filter.by.size' => 'Filter By Size',
    'coupon.code' => 'Coupon Code',
    'apply' => 'APPLY',
    'cart.totals' => 'Cart totals',
    'subtotal' => 'Subtotal',
    'coupon' => 'Coupon',
    'proceed.to.checkout' => 'Proceed To Checkout',
    'continue.shopping' => 'Continue Shopping',
    'continue' => 'Continue',
    'back' => 'Back',
    'login' => 'Login',
    'register' => 'Register',
    'forgot.password' => 'Forgot Password',
    'enter.your.password' => 'Enter Your Password',
    'enter.your.firstname' => 'Enter Your Firstname',
    'enter.your.lastname' => 'Enter Your Lastname',
    'choose.your.gender' => 'Choose Your Gender',
    'enter.your.phone.number' => 'Choose Your Phone Number',
    'male' => 'Male',
    'female' => 'Female',
    'create.account' => 'Create Account',
    'new.customer' => 'New Customer',
    'already.registered' => 'Already Registered?',
    'login.with.google' => 'Login with Google',
    'login.with.facebook' => 'Login with Facebook',
    'send.reset.password.link' => 'Send Reset Password Link',
    'reset.password' => 'Reset Password',
    'you.can.reset.your.password' => 'You can reset your password',
    'shipping.addresses' => 'Shipping Addresses',
    'billing.addresses' => 'Billing Addresses',
    'shipping.address' => 'Shipping Address',
    'shipping.details' => 'Shipping Details',
    'billing.details' => 'Billing Details',
    'billing.address' => 'Billing Address',
    'shipping.method' => 'Shipping Method',
    'payment' => 'Payment',
    'orders' => 'Orders',
    'profile' => 'Profile',
    'personal.information' => 'Personal Information',
    'update' => 'Update',
    'add' => 'Add',
    'delete' => 'Delete',
    'personal.information.updated' => 'Your personal information has been updated',
    'firstname' => 'Firstname',
    'lastname' => 'Lastname',
    'email' => 'Email',
    'title_text' => 'Title',
    'address' => 'Address',
    'select.country' => 'Select Country',
    'select.city' => 'Select City',
    'select.state' => 'Select State',
    'postal.code' => 'Postal Code',
    'add.address.title' => 'Add Address',
    'update.address.title' => 'Edit Address',
    'new.address.created' => 'New address has been created successfully',
    'address.updated' => 'Address has been updated successfully',
    'action' => 'Action',
    'add.new.address' => 'Add new address',
    'please.select.a.prefered.shipping.method' => 'Please select a prefered shipping method to use on this order',
    'shipping.fee' => 'Shipping Fee',
    'free' => 'Free',
    'shipping' => 'Shipping',
    'payment.methods' => 'Payment Methods',
    'orders.subtitle' => 'Orders list',
    'order.payment.pending.status' => 'Pending',
    'order.paid.status' => 'Paid',
    'order.error.payment.status' => 'Error in payment',
    'order.preparing.status' => 'Preparing',
    'order.send.to.cargo.status' => 'Send to cargo',
    'order.delivered.status' => 'Delivered',
    'order.id' => 'Order ID',
    'order.status' => 'Order Status',
    'order.date' => 'Order Date',
    'payment.gateway' => 'Payment Gateway',
    'delete.from.wishlist' => 'Remove from wishlist',
    'in.stock' => 'In stock',
    'out.of.stock' => 'Out of stock',
    'out.stock' => 'Out stock',
    'title' => 'Title'
];
