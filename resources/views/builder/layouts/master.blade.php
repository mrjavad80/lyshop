<!doctype html>
<html class="no-js" lang="en">


<!-- /lyshope/lyshope/index.html 25 Dec 2021 10:49:38 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    @php($website_title = builder()->key('website_title'))
    <title>{{$website_title}}</title>
    @php($website_description = builder()->key('website_description'))
    <meta name="description" content="{{$website_description}}">
    @php($website_keywords = builder()->key('website_keywords'))
    <meta name="keywords" content="{{$website_keywords}}">

    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images/favicon.icon')}}">

    <!-- CSS
	============================================ -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/vendor/bootstrap.min.css')}}">
    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/vendor/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/vendor/plaza-font.css')}}">

    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/plugins/slick.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/plugins/swiper.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/plugins/animation.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/plugins/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/plugins/fancy-box.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/plugins/jqueryui.min.css')}}">

    <!-- Vendor & Plugins CSS (Please remove the comment from below vendor.min.css & plugins.min.css for better website load performance and remove css files from avobe) -->
    <!--
    <script src="assets/js/vendor/vendor.min.js"></script>
    <script src="assets/js/plugins/plugins.min.js"></script>
    -->

    <!-- Main Style CSS (Please use minify version for better website load performance) -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <!--<link rel="stylesheet" href="assets/css/style.min.css">-->
    @include(render_view("layouts/theme-loader"))
</head>

<body>

@include(render_view("layouts/preloading"))
<div class="main-wrapper">


    @yield('content')

</div>

<!-- JS
============================================ -->
@yield('modals')
<script>
    let CONFIG_ROUTE_ADD_TO_CART = '{{route('add-to-cart', route_params())}}';
    let CONFIG_ROUTE_DELETE_FROM_CART = '{{route('delete-cart-item', route_params())}}';
    let CONFIG_ROUTE_GET_STATES = '{{route('get-states', route_params())}}';
    let CONFIG_ROUTE_GET_CITIES = '{{route('get-cities', route_params())}}';
    let CONFIG_ROUTE_ADD_TO_FAVORITE = '{{route('add-to-favorite', route_params())}}';
    let CONFIG_ROUTE_SUBMIT_PRODUCT_REVIEW = '{{route('submit-product-review', route_params(['json' => 1]))}}';
    let CONFIG_ROUTE_SUBMIT_POST_REVIEW = '{{route('submit-post-review', route_params(['json' => 1]))}}';
    let CONFIG_ROUTE_GENERATE_DATA_IN_TEMPLATE_HTML = '{{route('generate-data-template', route_params())}}';


    let LANG_PLEASE_SELECT_STATE = '{{__('content.select.state')}}';
    let LANG_PLEASE_SELECT_CITY = '{{__('content.select.city')}}';
    let LANG_ADD_TO_FAVORITE = '{{__('content.add.to.wishlist')}}';
    let LANG_DELETE_FROM_FAVORITE = '{{__('content.delete.from.wishlist')}}';

    let CONFIG_CURRENT_CURRENCY = '{!! json_encode(builder()->getDefaultCurrency()) !!}'
</script>
<!-- All custom scripts here -->
<script src="{{ asset('assets/js/scripts.js?v=1.8.0') }}"></script>

<!-- Modernizer JS -->
<script src="{{asset('assets/js/vendor/modernizr-3.6.0.min.js')}}"></script>
<!-- jQuery JS -->
<script src="{{asset('assets/js/vendor/jquery-3.3.1.min.js')}}"></script>
<!-- Bootstrap JS -->
<script src="{{asset('assets/js/vendor/popper.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/bootstrap.min.js')}}"></script>

<!-- Plugins JS -->
<script src="{{asset('assets/js/plugins/slick.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/swiper.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/countdown.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/image-zoom.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/fancybox.js')}}"></script>
<script src="{{asset('assets/js/plugins/scrollup.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/jqueryui.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/ajax-contact.js')}}"></script>

<!-- Vendor & Plugins JS (Please remove the comment from below vendor.min.js & plugins.min.js for better website load performance and remove js files from avobe) -->
<!--
<script src="assets/js/vendor/vendor.min.js"></script>
<script src="assets/js/plugins/plugins.min.js"></script>
-->

<!-- Main JS -->
<script src="assets/js/main.js"></script>

</body>


<!-- /lyshope/lyshope/index.html 25 Dec 2021 10:50:23 GMT -->
</html>
