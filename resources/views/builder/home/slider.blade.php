<div class="hero-slider-area">

    <div class="container">
        <div class="row">

            <div class="col-lg-3">
                <div class="categories-menu-wrap mt-30">
                    <div class="categories_menu">
                        <div class="categories_title">
                            <h5 class="categori_toggle">Categories</h5>
                        </div>
                        <div class="categories_menu_toggle">
                            <ul>
                                <li class="menu_item_children categorie_list"><a href="#">Ocean Food<i class="fa fa-angle-right"></i></a>
                                    <ul class="categories_mega_menu">
                                        <li><a href="#">Fish</a></li>
                                        <li><a href="#"> Shellfish</a></li>
                                        <li><a href="#">Roe</a></li>
                                    </ul>
                                </li>
                                <li class="menu_item_children"><a href="#">Butter & Eggs <i class="fa fa-angle-right"></i></a>
                                    <ul class="categories_mega_menu">
                                        <li><a href="#">Compound Butter</a></li>
                                        <li><a href="#">Cultured Butter</a></li>
                                        <li><a href="#">Whipped Butter</a></li>
                                    </ul>
                                </li>
                                <li class="menu_item_children"><a href="#">Dried Fruits <i class="fa fa-angle-right"></i></a>
                                    <ul class="categories_mega_menu">
                                        <li><a href="#">Mango</a></li>
                                        <li><a href="#">Plumsor</a></li>
                                        <li><a href="#">Raisins</a></li>
                                    </ul>
                                </li>
                                <li class="menu_item_children"><a href="#">Fast Food <i class="fa fa-angle-right"></i></a>
                                    <ul class="categories_mega_menu">
                                        <li><a href="#">Mango</a></li>
                                        <li><a href="#">Plumsor</a></li>
                                        <li><a href="#">Raisins</a></li>
                                    </ul>
                                </li>
                                <li class="menu_item_children"><a href="#">Fresh Meat <i class="fa fa-angle-right"></i></a>
                                    <ul class="categories_mega_menu">
                                        <li><a href="#">Mango</a></li>
                                        <li><a href="#">Plumsor</a></li>
                                        <li><a href="#">Raisins</a></li>
                                    </ul>
                                </li>
                                <li class="menu_item_children"><a href="#">Fruits <i class="fa fa-angle-right"></i></a>
                                    <ul class="categories_mega_menu">
                                        <li><a href="#">Mango</a></li>
                                    </ul>
                                </li>
                                <li class="menu_item_children"><a href="#">Milk & Cream <i class="fa fa-angle-right"></i></a>
                                    <ul class="categories_mega_menu">
                                        <li><a href="#">Mango</a></li>
                                        <li><a href="#">Plumsor</a></li>
                                    </ul>
                                </li>
                                <li class="menu_item_children"><a href="#">Vegetables <i class="fa fa-angle-right"></i></a>
                                    <ul class="categories_mega_menu">
                                        <li><a href="#">Mango</a></li>
                                        <li><a href="#">Raisins</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Prime Video</a>
                                </li>
                                <li class="hide-child"><a href="shop.html">Fruits</a></li>
                                <li class="categories-more-less ">
                                    <a class="more-default"><span class="c-more"></span>+ More Categories</a>
                                    <a class="less-show"><span class="c-more"></span>- Less Categories</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-9">
                <div class="hero-slider-wrapper mt-30">
                    <!-- Hero Slider Start -->
                    <div class="hero-slider-area hero-slider-one">
                        <div class="swiper-container gallery-top">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide" style="background-image:url(assets/images/slider/slide-bg-1.png)">
                                    <div class="hero-content-one">
                                        <div class="slider-content-text">
                                            <h2>Double BBQ <br>Bacon Cheese 2019 </h2>
                                            <p>Exclusive Offer -20% Off This Week </p>
                                            <div class="slider-btn">
                                                <a href="#">shopping Now</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="swiper-slide" style="background-image:url(assets/images/slider/slide-bg-2.jpg)">
                                    <div class="hero-content-one">
                                        <div class="slider-content-text">
                                            <h2>ADAM Apple <br>Big Sale 20% Off </h2>
                                            <p>Exclusive Offer -20% Off This Week </p>
                                            <div class="slider-btn">
                                                <a href="#">shopping Now</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="swiper-slide" style="background-image:url(assets/images/slider/slide-bg-3.jpg)">
                                    <div class="hero-content-one">
                                        <div class="slider-content-text">
                                            <h2>The Smart <br> Way To Eat Nuts</h2>
                                            <p>Exclusive Offer -20% Off This Week </p>
                                            <div class="slider-btn">
                                                <a href="#">shopping Now</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="swiper-slide" style="background-image:url(assets/images/slider/slide-bg-4.jpg)">
                                    <div class="hero-content-one">
                                        <div class="slider-content-text">
                                            <h2>Fresh Fruits <br>Super Discount</h2>
                                            <p>Exclusive Offer -20% Off This Week </p>
                                            <div class="slider-btn">
                                                <a href="#">shopping Now</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <!-- Add Arrows -->
                            <!--<div class="swiper-button-next swiper-button-white"></div>
                    <div class="swiper-button-prev swiper-button-white"></div>-->

                            <div class="swiper-pagination"></div>
                        </div>
                        <div class="swiper-container gallery-thumbs">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="slider-thum-text"><span>Double BBQ Bacon Cheese 2019</span></div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="slider-thum-text"><span>ADAM Apple Big Sale 20% Off</span></div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="slider-thum-text"><span>The Smart  Way To Eat Nuts</span></div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="slider-thum-text"><span>Fresh Fruits Super Discount</span></div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- Hero Slider End -->
                </div>
            </div>
        </div>
    </div>


</div>
