<?php
if(env('WB_ENDPOINT')){
    $endpoint = env('WB_ENDPOINT');
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') ? "https://" : "http://";
    $endpoint = $protocol.$endpoint;
}else{
    $endpoint = env('WB_ENDPOINT', env('WB_AUTHORIZE_KEY').'.wpbuilder.ca'.'/wbuilder/api/master/v1');
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') ? "https://" : "http://";
    $endpoint = $protocol.$endpoint;
}

return [
    'authorize_key' => env('WB_AUTHORIZE_KEY', 'authorize_key'),
    "preview_mode" => env('WB_PREVIEW_MODE', true),
    "endpoint" => $endpoint
];